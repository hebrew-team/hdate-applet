#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) 2005, 2008 Yaacov Zamir

import gtk
import pygtk
import gobject
from hdate import *

import locale
import getopt, sys, os
import types, ConfigParser

gpl_text = """
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# setup config object
config = ConfigParser.ConfigParser()

# set defaults
config.add_section('settings')
config.set('settings', 'locale', 'he_IL.utf8')
config.set('settings', 'show_holiday', 'true')
config.set('settings', 'show_parasha', 'true')
config.set('settings', 'show_sunset_sunrise', 'true')
config.set('settings', 'image_path', '/usr/share/pixmaps/hdate-applet/')
config.set('settings', 'image_theme', 'yellow-moon-')
config.set('settings', 'calendar_app', 'ghcal-he')

# get/set settings from rc file
config.read([os.path.expanduser('~/.hdate_systray.cfg')])
config.write(open(os.path.expanduser('~/.hdate_systray.cfg'), 'wb'))

# set locale for hebrew (You can change that for your locale or set it to '' for system locale)
locale.setlocale (locale.LC_ALL, config.get('settings', 'locale'))

# create global objects
h = Hdate ()
clip = gtk.Clipboard()
status_icon = gtk.StatusIcon ()

# quit program
def on_quit (widget, data = None):
     gtk.main_quit ()

# on clicking the icon show the calendar
def on_button_clicked (obj, data = None):
     os.system (config.get('settings', 'calendar_app'))

# copy date string to clipboard
def on_copy (widget, data = None):
     clip.set_text(h.get_format_date (0))

# copy show about dialog
def on_about (widget, data = None):
     about = gtk.AboutDialog ()
     about.set_name ("LibHdate system tray icon")
     about.set_copyright ("(C) 2008 Yaacov Zamir")
     about.set_license (gpl_text)
     
     about.run ()
     about.destroy ()

# show popup menu
def on_popup_menu (widget, button, time, data = None):
     if button == 3:
         if data:
             data.show_all ()
             data.popup (None, None, None, 3, time)
     pass

# rewrite tooltip on timeout
def on_timeout():
	# reset the date
	h.set_gdate (0, 0, 0)
	
	# get today's date
	tooltip_text = h.get_format_date (0)
	
	# if this is a holiday print the holiday name
	if h.get_holyday() and config.get('settings', 'show_holiday') == 'true':
		tooltip_text +=  "\n" + h.get_holyday_string(False)
		
	# if this is saturday print parasha
	if h.get_day_of_the_week() == 7 and config.get('settings', 'show_parasha') == 'true':
		tooltip_text +=  "\n" + h.get_parasha_string(False)
		
	# get sunset and sunrise time
	if config.get('settings', 'show_sunset_sunrise') == 'true':
		sunset_h = h.get_sunset() / 60
		sunset_m = h.get_sunset() - sunset_h * 60
		sunset_str = str(sunset_h) + ":" + str(sunset_m)
		sunrise_h = h.get_sunrise() / 60
		sunrise_m = h.get_sunrise() - sunrise_h * 60 
		sunrise_str = str(sunrise_h) + ":" + str(sunrise_m)
		# print sunset and sunrise
		tooltip_text +=  "\n(" + sunrise_str + "-" + sunset_str + ")"
	
	# set the tooltip
	status_icon.set_tooltip (tooltip_text)
	
	# set moon image
	image_filename =  config.get('settings', 'image_path')
	if h.get_hday() < 10:
		image_filename += config.get('settings', 'image_theme') + '0' + str (h.get_hday()) + '.png'
	else:
		image_filename += config.get('settings', 'image_theme') + str (h.get_hday()) + '.png'
	if os.path.isfile (image_filename):
		status_icon.set_from_file (image_filename)
	
	return True

# set popup menu
menu = gtk.Menu()
menu_item = gtk.ImageMenuItem (gtk.STOCK_COPY)
menu_item.connect ('activate', on_copy, status_icon)
menu.append (menu_item)
menu_item = gtk.ImageMenuItem (gtk.STOCK_ABOUT)
menu_item.connect ('activate', on_about, status_icon)
menu.append (menu_item)
menu_item = gtk.ImageMenuItem (gtk.STOCK_QUIT)
menu_item.connect ('activate', on_quit, status_icon)
menu.append (menu_item)

# set the staus icon
status_icon.set_from_stock (gtk.STOCK_HOME)
status_icon.connect ('popup-menu', on_popup_menu, menu)
status_icon.connect ("activate", on_button_clicked)
status_icon.set_visible (True)

# set the timeout functions to rewrite the tooltip
gobject.timeout_add (10000, on_timeout)

# init the tooltip
on_timeout()

# run main loop
gtk.main ()

