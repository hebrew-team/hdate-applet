/*  hdate_applet - Hebrew calendar GNOME panel applet
 *
 *  Copyright (C) 2004-2008  Yaacov Zamir <kzamir@walla.co.il>
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/* for the evolution calendar */
#include <libecal/e-cal.h>

/* get events for day,month,year from evolution clendar */
int
get_ecal_events (ECal * ecal, char *events,
		 int day, int month, int year, int max_events_string_length);

int
open_ecal_calendar (ECal **ecal);

int 
close_ecal_calendar (ECal *ecal);
