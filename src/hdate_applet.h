/*  hdate_applet - Hebrew calendar GNOME panel applet
 *
 *  Copyright (C) 2004-2010  Yaacov Zamir <kzamir@walla.co.il>
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __HDATE_APPLET_H__
#define __HDATE_APPLET_H__

#include <panel-applet.h>
#include <libgnomeui/libgnomeui.h>
#include <libgnome/libgnome.h>

#include <hdate.h>

#define KEY_FIRST		 "first"
#define KEY_SHOW_MOON		 "show_moon"
#define KEY_SHOW_HEBREW 	 "show_hebrew"
#define KEY_SHOW_GENERAL 	 "show_general"
#define KEY_SHOW_GENERAL_LONG 	 "show_general_long"
#define KEY_SHOW_SUNSET		 "show_sunset"
#define KEY_SHOW_PARASHA		 "show_parasha"
#define KEY_USE_DIASPORA	 "diaspora"
#define KEY_USE_HEBREW_L	 "locale"
#define KEY_THEME		 "theme"
#define KEY_HE_LOCALE		 "he_locale"
#define KEY_LATITUDE		 "latitude"
#define KEY_LONGITUDE		 "longitude"
#define KEY_TIME_ZONE		 "time_zone"
#define KEY_SUMMER_CLOCK	 "summer_clock"

typedef struct _HdateData
{
	/* widgets */
	GtkWidget *applet;
	GtkTooltips *tooltips;
	GtkWidget *label;
	GtkWidget *toggle;
	GtkWidget *alignment;
	GtkWidget *moon_image;
	GtkWidget *hbox;
	GtkWidget *vbox;

	/* preferences */
	gint first;
	gint showmoon;
	gint showhebrew;
	gint showgeneral;
	gint showgeneral_long;
	gint showsunset;
	gint showparasha;
	gint use_hebrew_locale;
	gint use_diaspora;

	double latitude;
	double longitude;
	double time_zone;
	gint summer_clock;

	char theme[20];
	char user_locale[20];
	char he_locale[20];

	PanelAppletOrient orient;
	int size;

} HdateData;

#endif
