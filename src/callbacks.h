/*  hdate_applet - Hebrew calendar GNOME panel applet
 *
 *  Copyright (C) 2004-2010  Yaacov Zamir <kzamir@walla.co.il>
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALLBACKS_H__
#define __CALLBACKS_H__

#include <gnome.h>

#include "hdate_applet.h"

void
on_checkbutton_show_moon_toggled (GtkToggleButton * togglebutton,
				  HdateData * cd);

void
on_checkbutton_show_heb_toggled (GtkToggleButton * togglebutton,
				 HdateData * cd);

void on_entry_heb_locale_changed (GtkEditable * editable, HdateData * cd);

void
on_checkbutton_show_gen_toggled (GtkToggleButton * togglebutton,
				 HdateData * cd);

void
on_checkbutton_show_gen_long_toggled (GtkToggleButton * togglebutton,
				 HdateData * cd);

void
on_checkbutton_show_set_toggled (GtkToggleButton * togglebutton,
				 HdateData * cd);

void
on_checkbutton_heb_toggled (GtkToggleButton * togglebutton, HdateData * cd);

void
on_checkbutton_show_parasha_toggled (GtkToggleButton * togglebutton,
				     HdateData * cd);

void
on_checkbutton_diaspora_toggled (GtkToggleButton * togglebutton,
				 HdateData * cd);

void on_combo_entry_theme_changed (GtkEditable * editable, HdateData * cd);

gboolean
on_window_prop_delete_event (GtkWidget * widget,
			     GdkEvent * event, gpointer user_data);

void on_button_ok_clicked (GtkButton * button, gpointer user_data);

void
show_about_cb (BonoboUIComponent * uic,
	       HdateData * cd, const gchar * verbname);

void
show_propeties_cb (BonoboUIComponent * uic,
		   HdateData * cd, const gchar * verbname);

void
copy_date_cb (BonoboUIComponent * uic,
	      HdateData * cd, const gchar * verbname);

void
copy_en_date_cb (BonoboUIComponent * uic,
		 HdateData * cd, const gchar * verbname);

void
on_checkbutton_summer_toggled (GtkToggleButton * togglebutton,
			       HdateData * cd);

void on_entry_lat_changed (GtkEditable * editable, HdateData * cd);

void on_entry_long_changed (GtkEditable * editable, HdateData * cd);

void on_entry_tz_changed (GtkEditable * editable, HdateData * cd);

#endif
