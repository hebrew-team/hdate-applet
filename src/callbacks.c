/*  hdate_applet - Hebrew calendar GNOME panel applet
 *
 *  Copyright (C) 2004-2010  Yaacov Zamir <kzamir@walla.co.il>
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <panel-applet-gconf.h>

#include <hdate.h>

#include <locale.h>

#include "hdate_applet.h"
#include "interface.h"
#include "callbacks.h"

void
on_checkbutton_show_moon_toggled (GtkToggleButton * togglebutton,
				  HdateData * cd)
{
	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (togglebutton)))
	{
		cd->showmoon = FALSE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_SHOW_MOON, FALSE, NULL);

	}
	else
	{
		cd->showmoon = TRUE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_SHOW_MOON, TRUE, NULL);

	}
	hdate_applet_paint (cd);
}


void
on_checkbutton_show_heb_toggled (GtkToggleButton * togglebutton,
				 HdateData * cd)
{
	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (togglebutton)))
	{
		cd->showhebrew = FALSE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_SHOW_HEBREW, FALSE, NULL);

	}
	else
	{
		cd->showhebrew = TRUE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_SHOW_HEBREW, TRUE, NULL);

	}
	hdate_applet_paint (cd);

}

void
on_entry_heb_locale_changed (GtkEditable * editable, HdateData * cd)
{
	g_snprintf (cd->he_locale, 50, "%s",
		    gtk_entry_get_text (GTK_ENTRY (editable)));

	panel_applet_gconf_set_string (PANEL_APPLET (cd->applet),
				       KEY_HE_LOCALE, cd->he_locale, NULL);

	if (cd->use_hebrew_locale)
	{
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_USE_HEBREW_L, TRUE, NULL);
		setenv ("LANG", cd->he_locale, 1);
		setenv ("LANGUAGE", cd->he_locale, 1);
		setlocale (LC_MESSAGES, cd->he_locale);
		gtk_set_locale ();
	}
	
	hdate_applet_paint (cd);
}

void
on_checkbutton_show_gen_toggled (GtkToggleButton * togglebutton,
				 HdateData * cd)
{
	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (togglebutton)))
	{
		cd->showgeneral = FALSE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_SHOW_GENERAL, FALSE, NULL);

	}
	else
	{
		cd->showgeneral = TRUE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_SHOW_GENERAL, TRUE, NULL);

	}
	hdate_applet_paint (cd);
}

void
on_checkbutton_show_gen_long_toggled (GtkToggleButton * togglebutton,
				 HdateData * cd)
{
	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (togglebutton)))
	{
		cd->showgeneral_long = FALSE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_SHOW_GENERAL_LONG, FALSE, NULL);

	}
	else
	{
		cd->showgeneral_long = TRUE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_SHOW_GENERAL_LONG, TRUE, NULL);

	}
	hdate_applet_paint (cd);
}

void
on_checkbutton_show_set_toggled (GtkToggleButton * togglebutton,
				 HdateData * cd)
{
	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (togglebutton)))
	{
		cd->showsunset = FALSE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_SHOW_SUNSET, FALSE, NULL);

	}
	else
	{
		cd->showsunset = TRUE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_SHOW_SUNSET, TRUE, NULL);

	}
	hdate_applet_paint (cd);
}


void
on_checkbutton_heb_toggled (GtkToggleButton * togglebutton, HdateData * cd)
{
	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (togglebutton)))
	{
		cd->use_hebrew_locale = FALSE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_USE_HEBREW_L, FALSE, NULL);
		setenv ("LANG", cd->user_locale, 1);
		setenv ("LANGUAGE", cd->user_locale, 1);
		setlocale (LC_MESSAGES, cd->user_locale);
		gtk_set_locale ();
	}
	else
	{
		cd->use_hebrew_locale = TRUE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_USE_HEBREW_L, TRUE, NULL);
		setenv ("LANG", cd->he_locale, 1);
		setenv ("LANGUAGE", cd->he_locale, 1);
		setlocale (LC_MESSAGES, cd->he_locale);
		gtk_set_locale ();
	}
	hdate_applet_paint (cd);
}

void
on_checkbutton_show_parasha_toggled (GtkToggleButton * togglebutton,
				     HdateData * cd)
{
	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (togglebutton)))
	{
		cd->showparasha = FALSE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_SHOW_PARASHA, FALSE, NULL);

	}
	else
	{
		cd->showparasha = TRUE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_SHOW_PARASHA, TRUE, NULL);

	}
	hdate_applet_paint (cd);
}

void
on_checkbutton_diaspora_toggled (GtkToggleButton * togglebutton,
				 HdateData * cd)
{
	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (togglebutton)))
	{
		cd->use_diaspora = FALSE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_USE_DIASPORA, FALSE, NULL);

	}
	else
	{
		cd->use_diaspora = TRUE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_USE_DIASPORA, TRUE, NULL);

	}
	hdate_applet_paint (cd);
}

void
on_combo_entry_theme_changed (GtkEditable * editable, HdateData * cd)
{
	g_stpcpy (cd->theme, gtk_entry_get_text (GTK_ENTRY (editable)));
	panel_applet_gconf_set_string (PANEL_APPLET (cd->applet),
				       KEY_THEME, cd->theme, NULL);
	hdate_applet_paint (cd);
}

void
on_checkbutton_summer_toggled (GtkToggleButton * togglebutton, HdateData * cd)
{
	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (togglebutton)))
	{
		cd->summer_clock = FALSE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_SUMMER_CLOCK, FALSE, NULL);

	}
	else
	{
		cd->summer_clock = TRUE;
		panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
					    KEY_SUMMER_CLOCK, TRUE, NULL);

	}
	hdate_applet_paint (cd);
}

void
on_entry_lat_changed (GtkEditable * editable, HdateData * cd)
{
	double temp;

	temp = strtod (gtk_entry_get_text (GTK_ENTRY (editable)), NULL);
	if ((temp >= 0) && (temp <= 82))
	{
		cd->latitude = temp;
	}
	panel_applet_gconf_set_float (PANEL_APPLET (cd->applet),
				    KEY_LATITUDE, cd->latitude, NULL);
	hdate_applet_paint (cd);
}

void
on_entry_long_changed (GtkEditable * editable, HdateData * cd)
{
	double temp;

	temp = strtod (gtk_entry_get_text (GTK_ENTRY (editable)), NULL);
	if ((temp >= -360) && (temp < 360))
	{
		cd->longitude = temp;
	}
	panel_applet_gconf_set_float (PANEL_APPLET (cd->applet),
				    KEY_LONGITUDE, cd->longitude, NULL);
	hdate_applet_paint (cd);
}

void
on_entry_tz_changed (GtkEditable * editable, HdateData * cd)
{
	double temp;

	temp = strtod (gtk_entry_get_text (GTK_ENTRY (editable)), NULL);
	if ((temp >= -180) && (temp < 180))
	{
		cd->time_zone = temp;
	}
	panel_applet_gconf_set_float (PANEL_APPLET (cd->applet),
				    KEY_TIME_ZONE, cd->time_zone, NULL);
	hdate_applet_paint (cd);
}

gboolean
on_window_prop_delete_event (GtkWidget * widget,
			     GdkEvent * event, gpointer user_data)
{

	return FALSE;
}

void
show_about_cb (BonoboUIComponent * uic,
	       HdateData * cd, const gchar * verbname)
{
	GtkWidget *about;

	about = create_about_hdate ();
	gtk_widget_show (about);
	
}

void
show_propeties_cb (BonoboUIComponent * uic,
		   HdateData * cd, const gchar * verbname)
{
	GtkWidget *dialog;

	dialog = create_window_prop (cd);
	gtk_widget_show (dialog);

}

void
on_button_ok_clicked (GtkButton * button, gpointer user_data)
{
	GtkWidget *parent, *widget;

	widget = GTK_WIDGET (button);

	for (;;)
	{
		parent = widget->parent;
		if (parent == NULL)
			break;
		widget = parent;
	}
	gtk_widget_destroy (widget);
}

void
copy_date_cb (BonoboUIComponent * uic, HdateData * cd, const gchar * verbname)
{
	gchar date[500];
	hdate_struct h;

	hdate_set_gdate (&h, 0, 0, 0);
	g_snprintf (date, 500, hdate_get_format_date (&h, FALSE, FALSE));

	gtk_clipboard_set_text (gtk_clipboard_get (GDK_SELECTION_CLIPBOARD),
				date, -1);
}

void
copy_en_date_cb (BonoboUIComponent * uic,
		 HdateData * cd, const gchar * verbname)
{
	gchar date[500];
	hdate_struct h;
	gchar *lc_ctype;

	hdate_set_gdate (&h, 0, 0, 0);

	lc_ctype = g_strdup (setlocale (LC_MESSAGES, NULL));

	setenv ("LANG", cd->user_locale, 1);
	setenv ("LANGUAGE", cd->user_locale, 1);
	setlocale (LC_MESSAGES, cd->user_locale);

	g_snprintf (date, 500, hdate_get_format_date (&h, FALSE, FALSE));

	setenv ("LANG", lc_ctype, 1);
	setenv ("LANGUAGE", lc_ctype, 1);
	setlocale (LC_MESSAGES, lc_ctype);

	g_free (lc_ctype);

	gtk_clipboard_set_text (gtk_clipboard_get (GDK_SELECTION_CLIPBOARD),
				date, -1);
}
