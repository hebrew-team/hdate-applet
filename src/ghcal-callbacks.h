/*  hdate_applet - Hebrew calendar GNOME panel applet
 *
 *  Copyright (C) 2004-2008  Yaacov Zamir <kzamir@walla.co.il>
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <gtk/gtk.h>


gboolean
on_arrow_prev_month_button_press_event (GtkWidget * widget,
					GdkEventButton * event,
					gpointer user_data);

gboolean
on_arrow_next_month_button_press_event (GtkWidget * widget,
					GdkEventButton * event,
					gpointer user_data);

gboolean
on_arrow_prev_year_button_press_event (GtkWidget * widget,
				       GdkEventButton * event,
				       gpointer user_data);

gboolean
on_arrow_next_year_button_press_event (GtkWidget * widget,
				       GdkEventButton * event,
				       gpointer user_data);

gboolean
on_arrow_current_date_button_press_event (GtkWidget * widget,
					  GdkEventButton * event,
					  gpointer user_data);

gboolean on_label_day_of_month_button_press_event
	(GtkWidget * widget, GdkEventButton * event, gpointer user_data);

gboolean
on_window_delete_event (GtkWidget * widget,
			GdkEvent * event, gpointer user_data);

void set_calender_to_date ();
