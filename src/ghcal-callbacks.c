/*  hdate_applet - Hebrew calendar GNOME panel applet
 *
 *  Copyright (C) 2004-2008  Yaacov Zamir <kzamir@walla.co.il>
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/* for the evolution calendar */
#ifdef WITH_EVOLUTION
  #include <bonobo/bonobo-main.h>
  #include <stdlib.h>
  #include <libecal/e-cal.h>
  #include <libecal/e-cal-time-util.h>
#endif

#include <gtk/gtk.h>

#include "ghcal-callbacks.h"
#include "ghcal-interface.h"

/* for evolution calendar */
#ifdef WITH_EVOLUTION
  #include "ecal-client.h"
#endif

#define MAX_STRING_SIZE 250

gboolean
on_arrow_prev_month_button_press_event (GtkWidget *
					widget,
					GdkEventButton *
					event, gpointer user_data)
{
	int new_month;
	int new_year;
	/* set the new date */
	new_month = hdate_get_gmonth (&today) - 1;
	new_year = hdate_get_gyear (&today);
	/* if next year */
	if (new_month < 1)
	{
		new_month = 12;
		new_year = new_year - 1;
	}

	hdate_set_gdate (&today, hdate_get_gday (&today),
			 new_month, new_year);
	/* redraw */
	set_calender_to_date ();
	return FALSE;
}

gboolean
on_arrow_next_month_button_press_event (GtkWidget *
					widget,
					GdkEventButton *
					event, gpointer user_data)
{
	int new_month;
	int new_year;
	/* set the new date */
	new_month = hdate_get_gmonth (&today) + 1;
	new_year = hdate_get_gyear (&today);
	/* if next year */
	if (new_month > 12)
	{
		new_month = 1;
		new_year = new_year + 1;
	}

	hdate_set_gdate (&today, hdate_get_gday (&today),
			 new_month, new_year);
	/* redraw */
	set_calender_to_date ();
	return FALSE;
}


gboolean
on_arrow_prev_year_button_press_event (GtkWidget * widget,
				       GdkEventButton *
				       event, gpointer user_data)
{
	int new_month;
	int new_year;
	/* set the new date */
	new_month = hdate_get_gmonth (&today);
	new_year = hdate_get_gyear (&today) - 1;
	hdate_set_gdate (&today, hdate_get_gday (&today),
			 new_month, new_year);
	/* redraw */
	set_calender_to_date ();
	return FALSE;
}

gboolean
on_arrow_next_year_button_press_event (GtkWidget * widget,
				       GdkEventButton *
				       event, gpointer user_data)
{
	int new_month;
	int new_year;
	/* set the new date */
	new_month = hdate_get_gmonth (&today);
	new_year = hdate_get_gyear (&today) + 1;
	hdate_set_gdate (&today, hdate_get_gday (&today),
			 new_month, new_year);
	/* redraw */
	set_calender_to_date ();
	return FALSE;
}

gboolean
on_arrow_current_date_button_press_event (GtkWidget *
					  widget,
					  GdkEventButton *
					  event, gpointer user_data)
{
	hdate_set_gdate (&today, 0, 0, 0);
	/* redraw */
	set_calender_to_date ();
	return FALSE;
}


gboolean on_label_day_of_month_button_press_event
	(GtkWidget * widget, GdkEventButton * event, gpointer user_data)
{
	int index;
	/* get the date user pressed */
	index = *((int *) user_data);
	today = hdate_day_of_month[index];
	/* redraw */
	set_calender_to_date ();
	return FALSE;
}


gboolean
on_window_delete_event (GtkWidget * widget,
			GdkEvent * event, gpointer user_data)
{
	gtk_main_quit ();
	return FALSE;
}

void
set_calender_to_date ()
{
	int julian;
	int todays_julian;
	int holyday;
	int index;
	hdate_struct first_day_of_month;
	hdate_struct last_day_of_month;
	char text_buffer[MAX_STRING_SIZE];
	char tooltip_buffer[MAX_STRING_SIZE];
	int is_saturday;
	int is_friday;
	int is_holyday;
	GdkColor color;

	/* for evolution calendar events */
#ifdef WITH_EVOLUTION
  int is_ecal_event;
	ECal *ecal;
	char ecal_events[MAX_STRING_SIZE];
#endif
	
	/* set month and year lables */
	g_snprintf (text_buffer, 150, "%s",
		    hdate_get_month_string (hdate_get_gmonth
					    (&today), FALSE));
	gtk_label_set_markup (GTK_LABEL (label_month), text_buffer);
	g_snprintf (text_buffer, 150, "%d", hdate_get_gyear (&today));
	gtk_label_set_markup (GTK_LABEL (label_year), text_buffer);

	/* set the month table */
	hdate_set_gdate (&first_day_of_month, 1,
			 hdate_get_gmonth (&today), hdate_get_gyear (&today));
	julian = hdate_get_julian (&first_day_of_month) -
		hdate_get_day_of_the_week (&first_day_of_month);
	todays_julian = hdate_get_julian (&today);

#ifdef WITH_EVOLUTION
 	/* open default evolution calendar */
 	if (!open_ecal_calendar (&ecal))
 	{ 
 		g_print ("failed to open calendar\n");
 	}
#endif
	
	for (index = 0; index < 42; index++)
	{
		/* set the labels */
		julian++;
		hdate_set_jd (&hdate_day_of_month[index], julian);

		/* get last day in this month */
		if (hdate_get_gmonth (&today) ==
		    hdate_get_gmonth (&hdate_day_of_month[index]))
			last_day_of_month = hdate_day_of_month[index];

		/* set the day lable text */
		g_snprintf (text_buffer, 150, "%d %s",
			    hdate_get_gday (&hdate_day_of_month[index]),
			    hdate_get_int_string (hdate_get_hday
						  (&hdate_day_of_month
						   [index])));

		/* set the day tooltip text */
		g_snprintf (tooltip_buffer, 150, "%s",
			    hdate_get_format_date (&hdate_day_of_month
						   [index], FALSE, FALSE));

		/* enable tooltips */
		gtk_tooltips_enable (tooltip_day_of_month[index]);

		/* check for events */
		holyday =
			hdate_get_holyday (&hdate_day_of_month[index], FALSE);
		is_saturday = index % 7 == 6;
		is_friday = index % 7 == 5;
		is_holyday = hdate_get_holyday_type (holyday);

#ifdef WITH_EVOLUTION
 		/* check for evolution calendar event */
		is_ecal_event =
			get_ecal_events (ecal, ecal_events,
					 hdate_get_gday (&hdate_day_of_month
							 [index]),
					 hdate_get_gmonth (&hdate_day_of_month
							   [index]),
					 hdate_get_gyear (&hdate_day_of_month
							  [index]),
					 MAX_STRING_SIZE);
#endif
      
		/* if saturday */
		if (is_saturday)
		{
			/* set backgraund */
			gdk_color_parse ("Red", &color);
			gtk_widget_modify_fg (label_day_of_month[index],
					      GTK_STATE_NORMAL, &color);
			gdk_color_parse ("White", &color);
			gtk_widget_modify_bg (event_box_day_of_month[index],
					      GTK_STATE_NORMAL, &color);
			/* enable tool tips for parasha */
		}
		else		/* just a day */
		{
			/* set backgraund */
			gdk_color_parse ("Black", &color);
			gtk_widget_modify_fg (label_day_of_month[index],
					      GTK_STATE_NORMAL, &color);
			gdk_color_parse ("White", &color);
			gtk_widget_modify_bg (event_box_day_of_month[index],
					      GTK_STATE_NORMAL, &color);
		}

		/* if friday */
		if (is_friday)
		{
			/* TODO: enable tool tips for sun set */

		}

		/* if holiday */
		if (is_holyday == 0)
		{
			/* leave as is */
		}
		else
		{
			/* set backgraund */
			gdk_color_parse ("Black", &color);
			gtk_widget_modify_fg (label_day_of_month[index],
					      GTK_STATE_NORMAL, &color);
			gdk_color_parse ("Yellow", &color);
			gtk_widget_modify_bg (event_box_day_of_month[index],
					      GTK_STATE_NORMAL, &color);
			/* enable tool tips for events */
		}

#ifdef WITH_EVOLUTION
		/* if evolution event */
		if (is_ecal_event == 0)
		{
			/* leave as is */
		}
		else
		{
			/* set backgraund */
			gdk_color_parse ("Black", &color);
			gtk_widget_modify_fg (label_day_of_month[index],
					      GTK_STATE_NORMAL, &color);
			gdk_color_parse ("LightYellow", &color);
			gtk_widget_modify_bg (event_box_day_of_month[index],
					      GTK_STATE_NORMAL, &color);
			/* add evolution events to tooltips */
			g_strlcat (tooltip_buffer, ecal_events,
				   MAX_STRING_SIZE);
		}
#endif
    
		/* if today */
		if (julian == todays_julian)
		{
			/* set backgraund */
			gdk_color_parse ("White", &color);
			gtk_widget_modify_fg (label_day_of_month[index],
					      GTK_STATE_NORMAL, &color);
			gdk_color_parse ("LightBlue", &color);
			gtk_widget_modify_bg (event_box_day_of_month[index],
					      GTK_STATE_NORMAL, &color);
		}
		else
		{
			/* leave as is */
		}

		/* if not in this month */
		if (hdate_get_gmonth (&today) !=
		    hdate_get_gmonth (&hdate_day_of_month[index]))
		{
			/* set backgraund */
			gdk_color_parse ("LightGrey", &color);
			gtk_widget_modify_fg (label_day_of_month[index],
					      GTK_STATE_NORMAL, &color);
			gdk_color_parse ("white", &color);
			gtk_widget_modify_bg (event_box_day_of_month[index],
					      GTK_STATE_NORMAL, &color);
		}
		else
		{
			/* leave as is */
		}

		/* set the day of month lable */
		gtk_label_set_text (GTK_LABEL (label_day_of_month[index]),
				    text_buffer);
		/* set the tooltip of month lable */
		gtk_tooltips_set_tip (tooltip_day_of_month
				      [index],
				      event_box_day_of_month
				      [index], tooltip_buffer, "");
	}

#ifdef WITH_EVOLUTION
 	/* close the evolution calenda */
 	close_ecal_calendar (ecal);
#endif

	/* fill in the month and year lable */
	g_snprintf (text_buffer, 150,
		    "%s, %s - %s",
		    hdate_get_hebrew_month_string
		    (hdate_get_hmonth (&first_day_of_month),
		     FALSE),
		    hdate_get_hebrew_month_string
		    (hdate_get_hmonth (&last_day_of_month),
		     FALSE),
		    hdate_get_int_string (hdate_get_hyear
					  (&first_day_of_month)));
	gtk_label_set_markup (GTK_LABEL
			      (label_hebrew_month_and_year), text_buffer);
	
	return;
}
