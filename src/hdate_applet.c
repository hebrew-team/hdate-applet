/*  hdate_applet - Hebrew calendar GNOME panel applet
 *
 *  Copyright (C) 2004-2010  Yaacov Zamir <kzamir@walla.co.il>
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <panel-applet-gconf.h>
#include <panel-applet.h>
#include <libgnomeui/libgnomeui.h>
#include <libgnome/libgnome.h>

#include <hdate.h>

#include <locale.h>

#include "../config.h"

#include "hdate_applet.h"
#include "interface.h"
#include "callbacks.h"

#define TIME_INTERVAL	1000*60	// one min

static const char my_menu_xml[] =
  "<popup name=\"button3\">\n"
  "  <menuitem name=\"Clock Preferences Item\" verb=\"properties\" _label=\"_Properties\"\n"
  "   	     pixtype=\"stock\" pixname=\"gtk-properties\"/>\n"
  "  <menuitem name=\"Clock Preferences Item\" verb=\"copy\" _label=\"_Copy date\"\n"
  "   	     pixtype=\"stock\" pixname=\"gtk-copy\"/>\n"
  "  <menuitem name=\"Clock Preferences Item\" verb=\"copyEn\" _label=\"C_opy date (En)\"\n"
  "   	     pixtype=\"stock\" pixname=\"gtk-copy\"/>\n"
  "  <menuitem name=\"About Item\" verb=\"MyAbout\" _label=\"About\"\n"
  "            pixtype=\"stock\" pixname=\"gnome-stock-about\"/>\n"
  "</popup>\n";

char *translate_strings[] =
  { N_("_Properties"), N_("_Copy date"), N_("C_opy date (En)"),
  N_("About")
};

static const BonoboUIVerb my_menu_verbs[] = {
  BONOBO_UI_UNSAFE_VERB ("properties", show_propeties_cb),
  BONOBO_UI_UNSAFE_VERB ("copy", copy_date_cb),
  BONOBO_UI_UNSAFE_VERB ("copyEn", copy_en_date_cb),
  BONOBO_UI_UNSAFE_VERB ("MyAbout", show_about_cb),
  BONOBO_UI_VERB_END
};

static void
update_orient (HdateData * cd)
{
  gdouble new_angle;
  gdouble angle;

  if (cd->orient == PANEL_APPLET_ORIENT_LEFT)
    new_angle = 270;
  else if (cd->orient == PANEL_APPLET_ORIENT_RIGHT)
    new_angle = 90;
  else
    new_angle = 0;

  angle = gtk_label_get_angle (GTK_LABEL (cd->label));
  if (angle == 0 && new_angle == 270)
    {
      gtk_label_set_angle (GTK_LABEL (cd->label), new_angle);

      /* remove widgets from applet container */
      gtk_container_remove (GTK_CONTAINER (cd->hbox), cd->moon_image);
      gtk_container_remove (GTK_CONTAINER (cd->hbox), cd->toggle);
      gtk_container_remove (GTK_CONTAINER (cd->applet), cd->hbox);

      /* reinsert widgets to applet container */
      g_object_ref (G_OBJECT (cd->vbox));
      g_object_ref (G_OBJECT (cd->toggle));
      g_object_ref (G_OBJECT (cd->moon_image));
      gtk_container_add (GTK_CONTAINER (cd->applet), cd->vbox);
      gtk_container_add (GTK_CONTAINER (cd->vbox), cd->moon_image);
      gtk_container_add (GTK_CONTAINER (cd->vbox), cd->toggle);
    }
  if (angle == 90 && new_angle == 270)
    {
      gtk_label_set_angle (GTK_LABEL (cd->label), new_angle);

      /* remove widgets from applet container */
      gtk_container_remove (GTK_CONTAINER (cd->vbox), cd->moon_image);
      gtk_container_remove (GTK_CONTAINER (cd->vbox), cd->toggle);

      /* reinsert widgets to applet container */
      g_object_ref (G_OBJECT (cd->toggle));
      g_object_ref (G_OBJECT (cd->moon_image));
      gtk_container_add (GTK_CONTAINER (cd->vbox), cd->moon_image);
      gtk_container_add (GTK_CONTAINER (cd->vbox), cd->toggle);
    }
  if (angle == 0 && new_angle == 90)
    {
      gtk_label_set_angle (GTK_LABEL (cd->label), new_angle);

      /* remove widgets from applet container */
      gtk_container_remove (GTK_CONTAINER (cd->applet), cd->hbox);
      gtk_container_remove (GTK_CONTAINER (cd->hbox), cd->toggle);
      gtk_container_remove (GTK_CONTAINER (cd->hbox), cd->moon_image);

      /* reinsert widgets to applet container */
      g_object_ref (G_OBJECT (cd->vbox));
      g_object_ref (G_OBJECT (cd->toggle));
      g_object_ref (G_OBJECT (cd->moon_image));
      gtk_container_add (GTK_CONTAINER (cd->applet), cd->vbox);
      gtk_container_add (GTK_CONTAINER (cd->vbox), cd->toggle);
      gtk_container_add (GTK_CONTAINER (cd->vbox), cd->moon_image);

    }
  if (angle == 270 && new_angle == 90)
    {
      gtk_label_set_angle (GTK_LABEL (cd->label), new_angle);

      /* remove widgets from applet container */
      gtk_container_remove (GTK_CONTAINER (cd->vbox), cd->moon_image);
      gtk_container_remove (GTK_CONTAINER (cd->vbox), cd->toggle);

      /* reinsert widgets to applet container */
      g_object_ref (G_OBJECT (cd->toggle));
      g_object_ref (G_OBJECT (cd->moon_image));
      gtk_container_add (GTK_CONTAINER (cd->vbox), cd->toggle);
      gtk_container_add (GTK_CONTAINER (cd->vbox), cd->moon_image);
    }
  if (angle != 0 && new_angle == 0)
    {
      gtk_label_set_angle (GTK_LABEL (cd->label), new_angle);

      /* remove widgets from applet container */
      gtk_container_remove (GTK_CONTAINER (cd->applet), cd->vbox);
      gtk_container_remove (GTK_CONTAINER (cd->vbox), cd->toggle);
      gtk_container_remove (GTK_CONTAINER (cd->vbox), cd->moon_image);

      /* reinsert widgets to applet container */
      g_object_ref (G_OBJECT (cd->hbox));
      g_object_ref (G_OBJECT (cd->toggle));
      g_object_ref (G_OBJECT (cd->moon_image));
      gtk_container_add (GTK_CONTAINER (cd->applet), cd->hbox);
      gtk_container_add (GTK_CONTAINER (cd->hbox), cd->moon_image);
      gtk_container_add (GTK_CONTAINER (cd->hbox), cd->toggle);
    }
}

/* this is when the panel orientation changes */
static void
applet_change_orient (GtkWidget * applet, PanelAppletOrient orient,
		      HdateData * cd)
{
  if (orient == cd->orient)
    return;

  cd->orient = orient;
  update_orient (cd);

  gtk_widget_queue_resize (GTK_WIDGET (cd->hbox));

  hdate_applet_paint (cd);

}

/* this is when the panel size changes */
void
applet_change_pixel_size (PanelApplet * applet, gint size, HdateData * cd)
{

  cd->size = size;
  hdate_applet_paint (cd);

}

/* code is shamelessly stolen from the clock applet
 */
static void
applet_change_background (PanelApplet * applet,
			  PanelAppletBackgroundType type,
			  GdkColor * color, GdkPixmap * pixmap,
			  HdateData * cd)
{
  GtkRcStyle *rc_style;
  GtkStyle *style;

  /* reset style */
  gtk_widget_set_style (GTK_WIDGET (cd->applet), NULL);
  rc_style = gtk_rc_style_new ();
  gtk_widget_modify_style (GTK_WIDGET (cd->applet), rc_style);
  gtk_rc_style_unref (rc_style);

  switch (type)
    {
    case PANEL_NO_BACKGROUND:
      break;
    case PANEL_COLOR_BACKGROUND:
      gtk_widget_modify_bg (GTK_WIDGET (cd->applet), GTK_STATE_NORMAL, color);
      break;
    case PANEL_PIXMAP_BACKGROUND:
      style = gtk_style_copy (GTK_WIDGET (cd->applet)->style);
      if (style->bg_pixmap[GTK_STATE_NORMAL])
	g_object_unref (style->bg_pixmap[GTK_STATE_NORMAL]);
      style->bg_pixmap[GTK_STATE_NORMAL] = g_object_ref (pixmap);
      gtk_widget_set_style (GTK_WIDGET (cd->applet), style);
      g_object_unref (style);
      break;
    }
}

/* this function is copied from clock applet */
static gboolean
do_not_eat_button_press (GtkWidget * widget, GdkEventButton * event)
{
  if (event->button != 1)
    g_signal_stop_emission_by_name (widget, "button_press_event");

  return FALSE;
}

static void
button_pressed (GtkWidget * button, HdateData * cd)
{
  if (cd->use_hebrew_locale)
    g_spawn_command_line_async ("ghcal-he", NULL);
  else
    g_spawn_command_line_async ("ghcal", NULL);
}

/* stolen from clock applet :) */
static inline void
force_no_focus_padding (GtkWidget * widget)
{
  gboolean first_time = TRUE;

  if (first_time)
    {
      gtk_rc_parse_string ("\n"
			   "   style \"hdate-applet-button-style\"\n"
			   "   {\n"
			   "      GtkWidget::focus-line-width=0\n"
			   "      GtkWidget::focus-padding=0\n"
			   "   }\n"
			   "\n"
			   "    widget \"*.hdate-applet-button\" style \"hdate-applet-button-style\"\n"
			   "\n");
      first_time = FALSE;
    }

  gtk_widget_set_name (widget, "hdate-applet-button");
}

static gboolean
hdate_applet_fill (PanelApplet * applet, const gchar * iid, gpointer data)
{

  HdateData *cd;

  if (strcmp (iid, "OAFIID:HDateApplet") != 0)
    return FALSE;

  cd = g_new0 (HdateData, 1);

  cd->orient = PANEL_APPLET_ORIENT_UP;

  panel_applet_add_preferences (applet,
				"/schemas/apps/hdate_applet/prefs", NULL);
  panel_applet_set_flags (applet, PANEL_APPLET_EXPAND_MINOR);

  cd->applet = GTK_WIDGET (applet);
  gtk_container_set_border_width (GTK_CONTAINER (cd->applet), 0);

  cd->tooltips = gtk_tooltips_new ();
  cd->hbox = gtk_hbox_new (FALSE, 0);
  cd->vbox = gtk_vbox_new (FALSE, 0);

  gtk_widget_show (cd->hbox);
  gtk_widget_show (cd->vbox);

  g_object_ref (G_OBJECT (cd->hbox));
  gtk_container_add (GTK_CONTAINER (cd->applet), cd->hbox);

  cd->moon_image = gtk_image_new ();
  gtk_misc_set_padding (GTK_MISC (cd->moon_image), 10, 2);
  g_object_ref (G_OBJECT (cd->moon_image));
  gtk_container_add (GTK_CONTAINER (cd->hbox), cd->moon_image);

  cd->label = gtk_label_new ("");
  gtk_label_set_justify (GTK_LABEL (cd->label), GTK_JUSTIFY_CENTER);
  gtk_label_set_ellipsize (GTK_LABEL (cd->label), PANGO_ELLIPSIZE_NONE);
  gtk_label_set_selectable (GTK_LABEL (cd->label), FALSE);
  gtk_label_set_line_wrap (GTK_LABEL (cd->label), FALSE);

  cd->toggle = gtk_button_new ();
  gtk_container_set_resize_mode (GTK_CONTAINER (cd->toggle),
				 GTK_RESIZE_IMMEDIATE);
  gtk_button_set_relief (GTK_BUTTON (cd->toggle), GTK_RELIEF_NONE);
  gtk_container_set_border_width (GTK_CONTAINER (cd->toggle), 0);

  force_no_focus_padding (cd->toggle);

  cd->alignment = gtk_alignment_new (0.5, 0.5, 1.0, 1.0);
  gtk_container_add (GTK_CONTAINER (cd->alignment), cd->label);
  gtk_container_set_resize_mode (GTK_CONTAINER (cd->alignment),
				 GTK_RESIZE_IMMEDIATE);
  gtk_widget_show (cd->alignment);
  gtk_container_add (GTK_CONTAINER (cd->toggle), cd->alignment);

  g_signal_connect (cd->toggle, "button_press_event",
		    G_CALLBACK (do_not_eat_button_press), NULL);

  g_signal_connect (cd->toggle, "clicked", G_CALLBACK (button_pressed), cd);

  gtk_widget_show (cd->toggle);

  g_object_ref (G_OBJECT (cd->toggle));
  gtk_container_add (GTK_CONTAINER (cd->hbox), cd->toggle);

  /* set the user main locale */
  g_stpcpy (cd->user_locale, setlocale (LC_MESSAGES, NULL));

  cd->first = panel_applet_gconf_get_int (PANEL_APPLET (cd->applet),
					  KEY_FIRST, NULL);
  if (cd->first == FALSE)
    {
      panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
				  KEY_FIRST, TRUE, NULL);
      cd->showmoon = FALSE;
      panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
				  KEY_SHOW_MOON, cd->showmoon, NULL);
      cd->showhebrew = TRUE;
      panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
				  KEY_SHOW_HEBREW, cd->showhebrew, NULL);
      cd->showgeneral = TRUE;
      panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
				  KEY_SHOW_GENERAL, cd->showgeneral, NULL);
      cd->showgeneral_long = FALSE;
      panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
				  KEY_SHOW_GENERAL_LONG,
				  cd->showgeneral_long, NULL);
      cd->showsunset = TRUE;
      panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
				  KEY_SHOW_SUNSET, cd->showsunset, NULL);
      cd->showparasha = TRUE;
      panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
				  KEY_SHOW_PARASHA, cd->showparasha, NULL);
      cd->use_hebrew_locale = TRUE;
      panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
				  KEY_USE_HEBREW_L, cd->use_hebrew_locale,
				  NULL);
      g_stpcpy (cd->he_locale, "he_IL.utf8");
      panel_applet_gconf_set_string (PANEL_APPLET (cd->applet),
				     KEY_HE_LOCALE, cd->he_locale, NULL);

      if (cd->use_hebrew_locale)
	{
	  panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
				      KEY_USE_HEBREW_L, TRUE, NULL);
	  setenv ("LANG", cd->he_locale, 1);
	  setenv ("LANGUAGE", cd->he_locale, 1);
	  setlocale (LC_MESSAGES, cd->he_locale);
	  gtk_set_locale ();
	}

      cd->use_diaspora = FALSE;
      panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
				  KEY_USE_DIASPORA, FALSE, NULL);
      cd->latitude = 32;
      panel_applet_gconf_set_float (PANEL_APPLET (cd->applet),
				  KEY_LATITUDE, cd->latitude, NULL);
      cd->longitude = 34;
      panel_applet_gconf_set_float (PANEL_APPLET (cd->applet),
				  KEY_LONGITUDE, cd->longitude, NULL);
      cd->time_zone = 2;
      panel_applet_gconf_set_float (PANEL_APPLET (cd->applet),
				  KEY_TIME_ZONE, cd->time_zone, NULL);
      cd->summer_clock = FALSE;
      panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
				  KEY_SUMMER_CLOCK, FALSE, NULL);

      g_stpcpy (cd->theme, "yellow");
      panel_applet_gconf_set_string (PANEL_APPLET (cd->applet),
				     KEY_THEME, cd->theme, NULL);
      cd->size = 24;
    }
  else
    {
      cd->showmoon =
	panel_applet_gconf_get_int (PANEL_APPLET (cd->applet),
				    KEY_SHOW_MOON, NULL);
      cd->showhebrew =
	panel_applet_gconf_get_int (PANEL_APPLET (cd->applet),
				    KEY_SHOW_HEBREW, NULL);
      cd->showgeneral =
	panel_applet_gconf_get_int (PANEL_APPLET (cd->applet),
				    KEY_SHOW_GENERAL, NULL);
      cd->showgeneral_long =
	panel_applet_gconf_get_int (PANEL_APPLET (cd->applet),
				    KEY_SHOW_GENERAL_LONG, NULL);
      cd->showsunset =
	panel_applet_gconf_get_int (PANEL_APPLET (cd->applet),
				    KEY_SHOW_SUNSET, NULL);
      cd->use_hebrew_locale =
	panel_applet_gconf_get_int (PANEL_APPLET (cd->applet),
				    KEY_USE_HEBREW_L, NULL);
      cd->showparasha =
	panel_applet_gconf_get_int (PANEL_APPLET (cd->applet),
				    KEY_SHOW_PARASHA, NULL);
      cd->use_diaspora =
	panel_applet_gconf_get_int (PANEL_APPLET (cd->applet),
				    KEY_USE_DIASPORA, NULL);

      g_stpcpy (cd->he_locale,
		panel_applet_gconf_get_string (PANEL_APPLET
					       (cd->applet),
					       KEY_HE_LOCALE, NULL));

      if (cd->use_hebrew_locale)
	{
	  panel_applet_gconf_set_int (PANEL_APPLET (cd->applet),
				      KEY_USE_HEBREW_L, TRUE, NULL);
	  setenv ("LANG", cd->he_locale, 1);
	  setenv ("LANGUAGE", cd->he_locale, 1);
	  setlocale (LC_MESSAGES, cd->he_locale);
	  gtk_set_locale ();
	}

      cd->latitude =
	panel_applet_gconf_get_float (PANEL_APPLET (cd->applet),
				    KEY_LATITUDE, NULL);
      cd->longitude =
	panel_applet_gconf_get_float (PANEL_APPLET (cd->applet),
				    KEY_LONGITUDE, NULL);
      cd->time_zone =
	panel_applet_gconf_get_float (PANEL_APPLET (cd->applet),
				    KEY_TIME_ZONE, NULL);
      cd->summer_clock =
	panel_applet_gconf_get_int (PANEL_APPLET (cd->applet),
				    KEY_SUMMER_CLOCK, NULL);

      g_stpcpy (cd->theme,
		panel_applet_gconf_get_string (PANEL_APPLET
					       (cd->applet),
					       KEY_THEME, NULL));
      cd->size = 24;
    }

  /* try and catch the initial orientation change */
  g_signal_connect (G_OBJECT (cd->applet),
		    "change_orient", G_CALLBACK (applet_change_orient), cd);

  g_signal_connect (G_OBJECT (cd->applet),
		    "change_size", G_CALLBACK (applet_change_pixel_size), cd);

  g_signal_connect (G_OBJECT (cd->applet),
		    "change_background",
		    G_CALLBACK (applet_change_background), cd);

  panel_applet_setup_menu (PANEL_APPLET (cd->applet),
			   my_menu_xml, my_menu_verbs, cd);

  gtk_timeout_add (TIME_INTERVAL, (GtkFunction) hdate_applet_paint,
		   (HdateData *) cd);

  hdate_applet_paint (cd);

  gtk_widget_show_all (GTK_WIDGET (cd->applet));

  /* show all will show moon image, but we do not want that */
  if (cd->showmoon)
    {
      gtk_widget_show (GTK_WIDGET (cd->moon_image));
    }
  else
    {
      gtk_widget_hide (GTK_WIDGET (cd->moon_image));
    }

  /* set initial orentation */
  applet_change_orient (GTK_WIDGET(cd->applet),
                        panel_applet_get_orient (PANEL_APPLET(cd->applet)), cd);

  return TRUE;
}

PANEL_APPLET_BONOBO_FACTORY ("OAFIID:HDateApplet_Factory",
			     PANEL_TYPE_APPLET,
			     "HDate Applet", "0", hdate_applet_fill, NULL);
