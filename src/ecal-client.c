
/*  hdate_applet - Hebrew calendar GNOME panel applet
 *
 *  Copyright (C) 2004-2008  Yaacov Zamir <kzamir@walla.co.il>
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/* for the evolution calendar */
#include <stdlib.h>
#include <libecal/e-cal.h>
#include <libecal/e-cal-time-util.h>

/* */

#include <gtk/gtk.h>

/* make_isodate_for_day_begin */
static inline char *
make_isodate_for_day_begin (int day, int month, int year)
{
  gchar *time_string;

  if (year > 9000 || year < 0)
    return NULL;

  if (month > 12 || month < 1)
    return NULL;

  if (day > 31 || day < 1)
    return NULL;

  time_string = g_strdup_printf ("%04d%02d%02dT000000Z", year, month, day);

  return time_string;
}

/* get events for day,month,year from evolution clendar */
int
get_ecal_events (ECal * ecal, char *events,
  int day, int month, int year, int max_events_string_length)
{
  GList *l, *objects;
  ECalComponentText summary;
  int number_of_events;
  char *str;

  /* strings for ecal query */
  char *query;
  char *day_begin;
  char *day_end;

  /* create iso time strings from locale time */
  day_begin = make_isodate_for_day_begin (day, month, year);
  day_end = make_isodate_for_day_begin (day + 1, month, year);

  /* create query string */
  query = g_strdup_printf
    ("(occur-in-time-range? (make-time \"%s\") "
    "(make-time \"%s\"))", day_begin, day_end);

  /* free the date strings */
  g_free (day_begin);
  g_free (day_end);

  /* send do query */
  if (!e_cal_get_object_list_as_comp (ecal, query, &objects, NULL))
  {
    /* free the query string */
    g_free (query);

    /* can not get events */
    g_print ("failed to get objects\n");
    number_of_events = 0;
  }
  else
  {
    /* free the query string */
    g_free (query);

    /* init the events */
    number_of_events = g_list_length (objects);
    g_snprintf (events, max_events_string_length, "");

    /* fill in all the events */
    for (l = objects; l; l = l->next)
    {
      ECalComponent *comp = E_CAL_COMPONENT (l->data);

      e_cal_component_get_summary (comp, &summary);
      str = g_strdup (summary.value);
      g_strlcat (events, "\n", max_events_string_length);
      g_strlcat (events, str, max_events_string_length);

      /* unreff the string and the calendar component */
      g_free (str);
      g_object_unref (comp);
    }

    /* free the component list */
    g_list_free (objects);
  }

  /* return the number of events */
  return number_of_events;
}

int
open_ecal_calendar (ECal ** ecal)
{
  char *uri = NULL;

  uri = g_build_filename (g_get_home_dir (), ".evolution",
    "calendar", "local", "system", NULL);
  (*ecal) = e_cal_new_from_uri (uri, E_CAL_SOURCE_TYPE_EVENT);
  g_free (uri);

  if (!e_cal_open ((*ecal), TRUE, NULL))
  {
    g_print ("failed to open calendar\n");
    return 0;
  }

  return -1;
}

int
close_ecal_calendar (ECal * ecal)
{
  g_object_unref (ecal);
  return 0;
}
