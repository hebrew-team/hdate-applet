/*  hdate_applet - Hebrew calendar GNOME panel applet
 *
 *  Copyright (C) 2004-2010  Yaacov Zamir <kzamir@walla.co.il>
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gnome.h>
#include <locale.h>
#include <hdate.h>

#include "hdate_applet.h"
#include "interface.h"
#include "callbacks.h"

#include "../config.h"

#undef _
#define _(String) dgettext (GETTEXT_PACKAGE, String)

GtkWidget *
create_window_prop (HdateData * cd)
{
	gchar text[500];

	GtkWidget *window_prop;
	GtkWidget *vbox_prop;
	GtkWidget *notebook_prp;
	GtkWidget *vbox1;
	GtkWidget *checkbutton_show_moon;
	GtkWidget *checkbutton_show_heb;
	GtkWidget *checkbutton_show_gen;
	GtkWidget *checkbutton_show_gen_long;
	GtkWidget *checkbutton_show_set;
	GtkWidget *checkbutton_show_parasha;
	GtkWidget *checkbutton_diaspora;
	GtkWidget *hbox_he_locale;
	GtkWidget *checkbutton_heb;
	GtkWidget *entry_heb_locale;
	GtkWidget *label_form;
	GtkWidget *vbox2;
	GtkWidget *label_theme;
	GtkWidget *combo1;
	GList *combo1_items = NULL;
	GtkWidget *combo_entry_theme;
	GtkWidget *label_imag;
	GtkWidget *table1;
	GtkWidget *label6;
	GtkWidget *label7;
	GtkWidget *label8;
	GtkWidget *checkbutton_summer;
	GtkWidget *entry_lat;
	GtkWidget *entry_long;
	GtkWidget *entry_tz;
	GtkWidget *label_loc;
	GtkWidget *button_ok;

	bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");

	window_prop = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (window_prop),
			      _("Hdate applet properties"));

	vbox_prop = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox_prop);
	gtk_container_add (GTK_CONTAINER (window_prop), vbox_prop);

	notebook_prp = gtk_notebook_new ();
	gtk_widget_show (notebook_prp);
	gtk_box_pack_start (GTK_BOX (vbox_prop), notebook_prp, TRUE, TRUE, 0);

	vbox1 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox1);
	gtk_container_add (GTK_CONTAINER (notebook_prp), vbox1);

	checkbutton_show_moon =
		gtk_check_button_new_with_mnemonic (_("Show moon image"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
				      (checkbutton_show_moon), cd->showmoon);

	gtk_widget_show (checkbutton_show_moon);
	gtk_box_pack_start (GTK_BOX (vbox1), checkbutton_show_moon, FALSE,
			    FALSE, 0);

	checkbutton_show_heb =
		gtk_check_button_new_with_mnemonic (_("Show hebrew date"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
				      (checkbutton_show_heb), cd->showhebrew);

	gtk_widget_show (checkbutton_show_heb);
	gtk_box_pack_start (GTK_BOX (vbox1), checkbutton_show_heb, FALSE,
			    FALSE, 0);

	checkbutton_show_gen =
		gtk_check_button_new_with_mnemonic (_
						    ("Show Greg. date (in tooltip)"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
				      (checkbutton_show_gen),
				      cd->showgeneral);

	gtk_widget_show (checkbutton_show_gen);
	gtk_box_pack_start (GTK_BOX (vbox1), checkbutton_show_gen, FALSE,
			    FALSE, 0);

	checkbutton_show_gen_long =
		gtk_check_button_new_with_mnemonic (_
						    ("Show the Greg. date using long format (in tooltip)"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
				      (checkbutton_show_gen_long),
				      cd->showgeneral_long);

	gtk_widget_show (checkbutton_show_gen_long);
	gtk_box_pack_start (GTK_BOX (vbox1), checkbutton_show_gen_long, FALSE,
			    FALSE, 0);

	checkbutton_show_set =
		gtk_check_button_new_with_mnemonic (_
						    ("Show sunset/rise time (in tooltip)"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
				      (checkbutton_show_set), cd->showsunset);

	gtk_widget_show (checkbutton_show_set);
	gtk_box_pack_start (GTK_BOX (vbox1), checkbutton_show_set, FALSE,
			    FALSE, 0);

	hbox_he_locale = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox_he_locale);
	gtk_container_add (GTK_CONTAINER (vbox1), hbox_he_locale);

	checkbutton_heb =
		gtk_check_button_new_with_mnemonic (_("Use locale (hebrew)"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (checkbutton_heb),
				      cd->use_hebrew_locale);

	gtk_widget_show (checkbutton_heb);
	gtk_box_pack_start (GTK_BOX (hbox_he_locale), checkbutton_heb, FALSE,
			    FALSE, 0);

	entry_heb_locale = gtk_entry_new ();
	gtk_widget_show (entry_heb_locale);
	gtk_box_pack_start (GTK_BOX (hbox_he_locale), entry_heb_locale, TRUE,
			    FALSE, 0);
	g_snprintf (text, 50, "%s", cd->he_locale);
	gtk_entry_set_text (GTK_ENTRY (entry_heb_locale), text);

	checkbutton_show_parasha =
		gtk_check_button_new_with_mnemonic (_("Show parasha"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
				      (checkbutton_show_parasha),
				      cd->showparasha);

	gtk_widget_show (checkbutton_show_parasha);
	gtk_box_pack_start (GTK_BOX (vbox1), checkbutton_show_parasha, FALSE,
			    FALSE, 0);

	checkbutton_diaspora =
		gtk_check_button_new_with_mnemonic (_
						    ("Use diaspra holidays and parashot"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
				      (checkbutton_diaspora),
				      cd->use_diaspora);

	gtk_widget_show (checkbutton_diaspora);
	gtk_box_pack_start (GTK_BOX (vbox1), checkbutton_diaspora, FALSE,
			    FALSE, 0);

	label_form = gtk_label_new (_("Label format"));
	gtk_widget_show (label_form);
	gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook_prp),
				    gtk_notebook_get_nth_page (GTK_NOTEBOOK
							       (notebook_prp),
							       0),
				    label_form);
	gtk_label_set_justify (GTK_LABEL (label_form), GTK_JUSTIFY_LEFT);

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
	gtk_container_add (GTK_CONTAINER (notebook_prp), vbox2);

	label_theme = gtk_label_new (_("Use Theme:"));
	gtk_widget_show (label_theme);
	gtk_box_pack_start (GTK_BOX (vbox2), label_theme, FALSE, FALSE, 0);
	gtk_label_set_justify (GTK_LABEL (label_theme), GTK_JUSTIFY_LEFT);

	combo1 = gtk_combo_new ();
	gtk_object_set_data (GTK_OBJECT (GTK_COMBO (combo1)->popwin),
			     "GladeParentKey", combo1);
	gtk_widget_show (combo1);
	gtk_box_pack_start (GTK_BOX (vbox2), combo1, FALSE, FALSE, 0);
	gtk_combo_set_value_in_list (GTK_COMBO (combo1), TRUE, FALSE);
	gtk_combo_set_use_arrows_always (GTK_COMBO (combo1), TRUE);
	combo1_items = g_list_append (combo1_items, (gpointer) ("yellow"));
	combo1_items = g_list_append (combo1_items, (gpointer) ("gray"));

	gtk_combo_set_popdown_strings (GTK_COMBO (combo1), combo1_items);
	g_list_free (combo1_items);

	combo_entry_theme = GTK_COMBO (combo1)->entry;
	gtk_entry_set_text (GTK_ENTRY (combo_entry_theme), cd->theme);

	gtk_widget_show (combo_entry_theme);
	gtk_entry_set_editable (GTK_ENTRY (combo_entry_theme), FALSE);

	label_imag = gtk_label_new (_("Image theme"));
	gtk_widget_show (label_imag);
	gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook_prp),
				    gtk_notebook_get_nth_page (GTK_NOTEBOOK
							       (notebook_prp),
							       1),
				    label_imag);
	gtk_label_set_justify (GTK_LABEL (label_imag), GTK_JUSTIFY_LEFT);

	table1 = gtk_table_new (5, 2, FALSE);
	gtk_widget_show (table1);
	gtk_container_add (GTK_CONTAINER (notebook_prp), table1);

	label6 = gtk_label_new (_("Latitude (decimal degrees):"));
	gtk_widget_show (label6);
	gtk_table_attach (GTK_TABLE (table1), label6, 0, 1, 0, 1,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_label_set_justify (GTK_LABEL (label6), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label6), 0, 0.5);

	label7 = gtk_label_new (_("Longitude (decimal degrees):"));
	gtk_widget_show (label7);
	gtk_table_attach (GTK_TABLE (table1), label7, 0, 1, 1, 2,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_label_set_justify (GTK_LABEL (label7), GTK_JUSTIFY_LEFT);
	gtk_misc_set_alignment (GTK_MISC (label7), 0, 0.5);

	label8 = gtk_label_new (_("Time zone:"));
	gtk_widget_show (label8);
	gtk_table_attach (GTK_TABLE (table1), label8, 0, 1, 2, 3,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_label_set_justify (GTK_LABEL (label8), GTK_JUSTIFY_LEFT);
	gtk_label_set_line_wrap (GTK_LABEL (label8), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label8), 0, 0.5);

	checkbutton_summer =
		gtk_check_button_new_with_mnemonic (_
						    ("Use summenr clock (+one hour)"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (checkbutton_summer),
				      cd->summer_clock);

	gtk_widget_show (checkbutton_summer);
	gtk_table_attach (GTK_TABLE (table1), checkbutton_summer, 0, 1, 3, 4,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);

	entry_lat = gtk_entry_new ();
	gtk_widget_show (entry_lat);
	gtk_table_attach (GTK_TABLE (table1), entry_lat, 1, 2, 0, 1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	g_snprintf (text, 50, "%.2f", cd->latitude);
	gtk_entry_set_text (GTK_ENTRY (entry_lat), text);

	entry_long = gtk_entry_new ();
	gtk_widget_show (entry_long);
	gtk_table_attach (GTK_TABLE (table1), entry_long, 1, 2, 1, 2,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	g_snprintf (text, 50, "%.2f", cd->longitude);
	gtk_entry_set_text (GTK_ENTRY (entry_long), text);

	entry_tz = gtk_entry_new ();
	gtk_widget_show (entry_tz);
	gtk_table_attach (GTK_TABLE (table1), entry_tz, 1, 2, 2, 3,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	g_snprintf (text, 50, "%.2f", cd->time_zone);
	gtk_entry_set_text (GTK_ENTRY (entry_tz), text);

	label_loc = gtk_label_new (_("Location"));
	gtk_widget_show (label_loc);
	gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook_prp),
				    gtk_notebook_get_nth_page (GTK_NOTEBOOK
							       (notebook_prp),
							       2), label_loc);
	gtk_label_set_justify (GTK_LABEL (label_loc), GTK_JUSTIFY_LEFT);

	button_ok = gtk_button_new_from_stock ("gtk-ok");
	gtk_widget_show (button_ok);
	gtk_box_pack_start (GTK_BOX (vbox_prop), button_ok, TRUE, TRUE, 0);

	gtk_signal_connect (GTK_OBJECT (window_prop), "delete_event",
			    GTK_SIGNAL_FUNC (on_window_prop_delete_event),
			    cd);
	gtk_signal_connect (GTK_OBJECT (checkbutton_show_moon), "toggled",
			    GTK_SIGNAL_FUNC
			    (on_checkbutton_show_moon_toggled), cd);
	gtk_signal_connect (GTK_OBJECT (checkbutton_show_heb), "toggled",
			    GTK_SIGNAL_FUNC (on_checkbutton_show_heb_toggled),
			    cd);
	gtk_signal_connect (GTK_OBJECT (entry_heb_locale), "changed",
			    GTK_SIGNAL_FUNC (on_entry_heb_locale_changed),
			    cd);
	gtk_signal_connect (GTK_OBJECT (checkbutton_show_gen), "toggled",
			    GTK_SIGNAL_FUNC (on_checkbutton_show_gen_toggled),
			    cd);
	gtk_signal_connect (GTK_OBJECT (checkbutton_show_gen_long), "toggled",
			    GTK_SIGNAL_FUNC
			    (on_checkbutton_show_gen_long_toggled), cd);
	gtk_signal_connect (GTK_OBJECT (checkbutton_show_set), "toggled",
			    GTK_SIGNAL_FUNC (on_checkbutton_show_set_toggled),
			    cd);
	gtk_signal_connect (GTK_OBJECT (checkbutton_show_parasha), "toggled",
			    GTK_SIGNAL_FUNC
			    (on_checkbutton_show_parasha_toggled), cd);
	gtk_signal_connect (GTK_OBJECT (checkbutton_heb), "toggled",
			    GTK_SIGNAL_FUNC (on_checkbutton_heb_toggled), cd);
	gtk_signal_connect (GTK_OBJECT (checkbutton_diaspora), "toggled",
			    GTK_SIGNAL_FUNC (on_checkbutton_diaspora_toggled),
			    cd);
	gtk_signal_connect (GTK_OBJECT (checkbutton_summer), "toggled",
			    GTK_SIGNAL_FUNC (on_checkbutton_summer_toggled),
			    cd);
	gtk_signal_connect (GTK_OBJECT (combo_entry_theme), "changed",
			    GTK_SIGNAL_FUNC (on_combo_entry_theme_changed),
			    cd);
	gtk_signal_connect (GTK_OBJECT (entry_lat), "changed",
			    GTK_SIGNAL_FUNC (on_entry_lat_changed), cd);
	gtk_signal_connect (GTK_OBJECT (entry_long), "changed",
			    GTK_SIGNAL_FUNC (on_entry_long_changed), cd);
	gtk_signal_connect (GTK_OBJECT (entry_tz), "changed",
			    GTK_SIGNAL_FUNC (on_entry_tz_changed), cd);
	gtk_signal_connect (GTK_OBJECT (button_ok), "clicked",
			    GTK_SIGNAL_FUNC (on_button_ok_clicked), cd);
	return window_prop;
}

GtkWidget *
create_about_hdate (void)
{
	bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");

	static const gchar *authors[] =
	{
		"Yaacov Zamir <kzamir@walla.co.il>",
		NULL
	};
	static const char *documenters[] =
	{
		"Yaacov Zamir <kzamir@walla.co.il>",
		NULL
	};

	GtkWidget * about_hdate = gtk_about_dialog_new ();
	g_object_set (about_hdate,
		      "name",  _("hdate-applet"),
		      "version", VERSION,
		      "copyright", "Copyright \xc2\xa9 2002-2005 Yaacov Zamir",
		      "comments", _("Hdate Applet displays the current Hebrew date"),
		      "authors", authors,
		      "documenters", documenters,
		      "translator-credits", _("translator_credits"),
		      "logo-icon-name", "clock",
		      NULL);
	
	/* close dialog on exit */	      
	g_signal_connect (about_hdate, "response", G_CALLBACK (gtk_widget_hide_on_delete), NULL);
	
	return about_hdate;
}


gint
hdate_applet_paint (HdateData * cd)
{

	gchar date[500];
	gchar temp[500];
	hdate_struct h, h_parasha;
	int moon_phase;
	GdkPixbuf *scaled;
	GdkPixbuf *pixbuf;
	int diaspora;
	int sunrise, sunset;

	diaspora = cd->use_diaspora;

	if (cd->showmoon)
	{
		gtk_widget_show (GTK_WIDGET (cd->moon_image));
	}
	else
	{
		gtk_widget_hide (GTK_WIDGET (cd->moon_image));
	}

	hdate_set_gdate (&h, 0, 0, 0);

	if (cd->showhebrew)
	{

		g_snprintf (date, 500, "%s",
			    hdate_get_format_date (&h, diaspora, TRUE));
	}
	else
	{
		g_snprintf (date, 500, " ");
	}
	
	gtk_label_set_text (GTK_LABEL (cd->label), date);

	if (cd->showgeneral)
	{
		if (cd->showgeneral_long)
		{
			g_snprintf (date, 500, "%s %d, %d\n",
				    hdate_get_month_string (h.gd_mon, FALSE),
				    h.gd_day, h.gd_year);
		}
		else
		{
			g_snprintf (date, 500, "%d.%d.%d\n", h.gd_day,
				    h.gd_mon, h.gd_year);
		}
	}
	else
	{
		g_snprintf (date, 500, "");
	}

	if (cd->showsunset)
	{

		/* get times */
		hdate_get_utc_sun_time (h.gd_day, h.gd_mon, h.gd_year,
					cd->latitude, cd->longitude, &sunrise,
					&sunset);

		sunset = sunset + cd->time_zone * 60;
		sunrise = sunrise + cd->time_zone * 60;

		if (cd->summer_clock)
		{
			sunrise += 60;
			sunset += 60;
		}

		g_snprintf (temp, 500, "(%d:%d - %d:%d)\n",
			    sunrise / 60, sunrise % 60, sunset / 60,
			    sunset % 60);

		g_strlcat (date, temp, 500);
	}

	if (cd->showhebrew)
	{
		g_strlcat (date, hdate_get_format_date (&h, diaspora, FALSE),
			   500);
	}

	if (cd->showparasha)
	{
		hdate_set_jd (&h_parasha, h.hd_jd - h.hd_dw + 7);
		if (hdate_get_parasha (&h_parasha, diaspora))
		{
			g_strlcat (date, "\n", 500);
			g_strlcat (date,
				   hdate_get_parasha_string (hdate_get_parasha
							     (&h_parasha,
							      diaspora),
							     FALSE), 500);
		}
	}

	gtk_tooltips_set_tip (cd->tooltips, GTK_WIDGET (cd->applet), date,
			      NULL);

	moon_phase = (h.hd_day) + 1;
	if (moon_phase < 1 || moon_phase > 28)
	{
		moon_phase = 28;
	}

	g_snprintf (date, 500, "%s/%s-moon-%02d.png", PIXMAP_DIR, cd->theme,
		    moon_phase);

	pixbuf = gdk_pixbuf_new_from_file (date, NULL);

	scaled = gdk_pixbuf_scale_simple (pixbuf,
					  panel_applet_get_size (PANEL_APPLET
								 (cd->
								  applet)) -
					  2,
					  panel_applet_get_size (PANEL_APPLET
								 (cd->
								  applet)) -
					  2, GDK_INTERP_BILINEAR);


	gtk_image_set_from_pixbuf (GTK_IMAGE (cd->moon_image), scaled);

	g_object_unref (scaled);
	g_object_unref (pixbuf);

	return (TRUE);
}
