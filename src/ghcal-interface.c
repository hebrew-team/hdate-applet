/*  hdate_applet - Hebrew calendar GNOME panel applet
 *
 *  Copyright (C) 2004-2008  Yaacov Zamir <kzamir@walla.co.il>
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <hdate.h>

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>

#include "ghcal-callbacks.h"
#include "ghcal-interface.h"

GtkWidget *
create_window (void)
{
	int index;
	/*      hdate_struct today; *//* the commented vars are global */
	char text_buffer[50];

	GtkWidget *window;
	GtkWidget *vbox_all;
	GtkWidget *hbox_month_and_year;

	GtkWidget *arrow_prev_month;
	GtkWidget *event_box_prev_month;
/*	GtkWidget *lable_month;*/
	GtkWidget *arrow_next_month;
	GtkWidget *event_box_next_month;
	GtkWidget *arrow_prev_year;
	GtkWidget *event_box_prev_year;
/*	GtkWidget *label_year;*/
	GtkWidget *arrow_next_year;
	GtkWidget *event_box_next_year;

/*	GtkWidget *entry_hebrew_date;*/
/*	GtkWidget *label_hebrew_month_and_year;*/
	GtkWidget *arrow_current_date;
	GtkWidget *event_box_current_date;

	GtkWidget *hbox_days_of_week;

	GtkWidget *event_box_day_of_week[7];
	GtkWidget *label_day_of_week[7];
	GtkWidget *table_days_of_month;

/*	int index_day_of_month[42];*/
/*	hdate_struct hdate_day_of_month[42];*/
/*	GtkWidget *label_day_of_month[42];*/
/*	GtkWidget *event_box_day_of_month[42];*/
/*	GtkTooltips *tooltip_day_of_month[42];*/

	GdkColor color;

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (window), _("gHcal - Hebrew Calendar"));

	vbox_all = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox_all);
	gtk_container_add (GTK_CONTAINER (window), vbox_all);

	hbox_month_and_year = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox_month_and_year);
	gtk_box_pack_start (GTK_BOX (vbox_all), hbox_month_and_year, FALSE,
			    FALSE, 4);

	arrow_prev_month = gtk_arrow_new (GTK_ARROW_LEFT, GTK_SHADOW_OUT);
	gtk_widget_show (arrow_prev_month);
	event_box_prev_month = gtk_event_box_new ();
	gtk_widget_show (event_box_prev_month);
	gtk_container_add (GTK_CONTAINER
			   (event_box_prev_month), arrow_prev_month);
	gtk_box_pack_start (GTK_BOX (hbox_month_and_year),
			    event_box_prev_month, FALSE, FALSE, 5);

	/* set empty month lable */
	label_month = gtk_label_new ("*");
	gtk_label_set_use_markup (GTK_LABEL (label_month), TRUE);
	gtk_widget_set_size_request (GTK_WIDGET (label_month), 70, 20);
	gtk_widget_show (label_month);
	gtk_box_pack_start (GTK_BOX (hbox_month_and_year), label_month,
			    FALSE, TRUE, 0);

	arrow_next_month = gtk_arrow_new (GTK_ARROW_RIGHT, GTK_SHADOW_OUT);
	gtk_widget_show (arrow_next_month);
	event_box_next_month = gtk_event_box_new ();
	gtk_widget_show (event_box_next_month);
	gtk_container_add (GTK_CONTAINER
			   (event_box_next_month), arrow_next_month);
	gtk_box_pack_start (GTK_BOX (hbox_month_and_year),
			    event_box_next_month, FALSE, FALSE, 5);

	arrow_prev_year = gtk_arrow_new (GTK_ARROW_LEFT, GTK_SHADOW_OUT);
	gtk_widget_show (arrow_prev_year);
	event_box_prev_year = gtk_event_box_new ();
	gtk_widget_show (event_box_prev_year);
	gtk_container_add (GTK_CONTAINER
			   (event_box_prev_year), arrow_prev_year);
	gtk_box_pack_start (GTK_BOX (hbox_month_and_year),
			    event_box_prev_year, FALSE, FALSE, 5);

	/* set empty year lable */
	label_year = gtk_label_new ("*");
	gtk_label_set_use_markup (GTK_LABEL (label_year), TRUE);
	gtk_widget_show (label_year);
	gtk_box_pack_start (GTK_BOX (hbox_month_and_year), label_year,
			    FALSE, TRUE, 5);

	arrow_next_year = gtk_arrow_new (GTK_ARROW_RIGHT, GTK_SHADOW_OUT);
	gtk_widget_show (arrow_next_year);
	event_box_next_year = gtk_event_box_new ();
	gtk_widget_show (event_box_next_year);
	gtk_container_add (GTK_CONTAINER
			   (event_box_next_year), arrow_next_year);
	gtk_box_pack_start (GTK_BOX (hbox_month_and_year),
			    event_box_next_year, FALSE, FALSE, 5);

	/* set an empty hebrew month and year lable */
	label_hebrew_month_and_year = gtk_label_new ("*");
	gtk_misc_set_alignment (GTK_MISC (label_hebrew_month_and_year), 0.5,
				0);
	gtk_label_set_use_markup (GTK_LABEL (label_hebrew_month_and_year),
				  TRUE);
	gtk_widget_set_size_request (GTK_WIDGET (label_hebrew_month_and_year),
				     180, 20);
	gtk_widget_show (label_hebrew_month_and_year);
	gtk_box_pack_start (GTK_BOX (hbox_month_and_year),
			    label_hebrew_month_and_year, TRUE, FALSE, 5);

	arrow_current_date = gtk_arrow_new (GTK_ARROW_UP, GTK_SHADOW_OUT);
	gtk_widget_show (arrow_current_date);
	event_box_current_date = gtk_event_box_new ();
	gtk_widget_show (event_box_current_date);
	gtk_container_add (GTK_CONTAINER
			   (event_box_current_date), arrow_current_date);
	gtk_box_pack_start (GTK_BOX (hbox_month_and_year),
			    event_box_current_date, FALSE, FALSE, 5);

	hbox_days_of_week = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox_days_of_week);
	gtk_box_pack_start (GTK_BOX (vbox_all), hbox_days_of_week,
			    FALSE, FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (hbox_days_of_week), 0);

	/* header for days of the week */
	for (index = 0; index < 7; index++)
	{
		/* set the day of week header lable */
		g_snprintf (text_buffer, 50,
			    "<span size=\"medium\" weight=\"bold\">%s</span>",
			    hdate_get_day_string (index + 1, TRUE));
		label_day_of_week[index] = gtk_label_new (text_buffer);
		gtk_widget_set_size_request (GTK_WIDGET
					     (label_day_of_week[index]), 45,
					     20);
		event_box_day_of_week[index] = gtk_event_box_new ();
		gtk_widget_show (label_day_of_week[index]);
		gtk_widget_show (event_box_day_of_week[index]);

		gtk_container_add (GTK_CONTAINER
				   (event_box_day_of_week[index]),
				   label_day_of_week[index]);

		gtk_box_pack_start (GTK_BOX (hbox_days_of_week),
				    event_box_day_of_week[index], TRUE, TRUE,
				    0);
		gtk_label_set_use_markup (GTK_LABEL
					  (label_day_of_week[index]), TRUE);

		/* if shabat highligt day header */
		if (index == 6)
		{
			gdk_color_parse ("White", &color);
			gtk_widget_modify_fg (label_day_of_week[index],
					      GTK_STATE_NORMAL, &color);
			gdk_color_parse ("LightBlue", &color);
			gtk_widget_modify_bg (event_box_day_of_week[index],
					      GTK_STATE_NORMAL, &color);
		}
		else
		{
			gdk_color_parse ("White", &color);
			gtk_widget_modify_fg (label_day_of_week[index],
					      GTK_STATE_NORMAL, &color);
			gdk_color_parse ("LightBlue", &color);
			gtk_widget_modify_bg (event_box_day_of_week[index],
					      GTK_STATE_NORMAL, &color);
		}
	}

	table_days_of_month = gtk_table_new (6, 7, FALSE);
	gtk_widget_show (table_days_of_month);
	gtk_box_pack_start (GTK_BOX (vbox_all), table_days_of_month,
			    TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (table_days_of_month),
					0);

	/* create an empty month table */
	for (index = 0; index < 42; index++)
	{
		/* allocate new widgets */
		index_day_of_month[index] = index;
		event_box_day_of_month[index] = gtk_event_box_new ();
		label_day_of_month[index] = gtk_label_new ("*");
		gtk_widget_set_size_request (GTK_WIDGET
					     (label_day_of_month[index]), 45,
					     20);
		tooltip_day_of_month[index] = gtk_tooltips_new ();

		/* show new widgets */
		gtk_widget_show (label_day_of_month[index]);
		gtk_widget_show (event_box_day_of_month[index]);

		gtk_container_add (GTK_CONTAINER
				   (event_box_day_of_month[index]),
				   label_day_of_month[index]);

		gtk_table_attach_defaults (GTK_TABLE (table_days_of_month),
					   event_box_day_of_month[index],
					   index % 7, index % 7 + 1,
					   index / 7, index / 7 + 1);

		/* extra settings of widgets */
		gtk_label_set_use_markup (GTK_LABEL
					  (label_day_of_month[index]), TRUE);

		g_signal_connect ((gpointer) event_box_day_of_month[index],
				  "button_press_event",
				  G_CALLBACK
				  (on_label_day_of_month_button_press_event),
				  (gpointer) & index_day_of_month[index]);
	}

	g_signal_connect ((gpointer) window, "delete_event",
			  G_CALLBACK (on_window_delete_event), NULL);

	g_signal_connect ((gpointer) event_box_prev_month,
			  "button_press_event",
			  G_CALLBACK (on_arrow_prev_month_button_press_event),
			  NULL);

	g_signal_connect ((gpointer) event_box_next_month,
			  "button_press_event",
			  G_CALLBACK (on_arrow_next_month_button_press_event),
			  NULL);

	g_signal_connect ((gpointer) event_box_prev_year,
			  "button_press_event",
			  G_CALLBACK (on_arrow_prev_year_button_press_event),
			  NULL);

	g_signal_connect ((gpointer) event_box_next_year,
			  "button_press_event",
			  G_CALLBACK (on_arrow_next_year_button_press_event),
			  NULL);

	g_signal_connect ((gpointer) event_box_current_date,
			  "button_press_event",
			  G_CALLBACK
			  (on_arrow_current_date_button_press_event), NULL);

	/* fill the calendar for today */
	hdate_set_gdate (&today, 0, 0, 0);
	set_calender_to_date ();

	return window;
}
